module.exports = {
  redis: {
    host: process.env.REDIS_HOST || 'redis-srv',
    port: process.env.REDIS_PORT || 6379
  },
  port: process.env.PORT || 8080,
  debug: process.env.DEBUG && process.env.DEBUG === "true"
};
